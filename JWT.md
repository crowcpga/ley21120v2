# Ley 21120 

# (PIG) Proceso Identidad Genero

![](https://estaticos.muyinteresante.es/media/cache/1140x_thumb/uploads/images/gallery/5b0e72725cafe87d7a3c986b/mini-pig-manchas-morro.jpg)

## JWT (json web token)

### ¿Cómo funciona?

El funcionamiento de JWT es bastante simple y se basa en 6 pasos:

1. Primero: autentica usando credenciales regulares (usuario-contraseña normalmente)
2. Segundo: Una vez que ya autenticó en el servidor, genera una cadena de caracteres que contiene la token de JWT integrada.
3. Tercero: Esa token es enviada al cliente
4. Cuarto: La token se almacena en el lado del cliente
5. Quinto: La token se manda al lado del servidor en cada petición que se realiza.
6. Sexto: El servidor valida la token y otorga (o no) acceso al recurso que el cliente solicita.













### Implementacion:

Autentificar usando usuario y contraseña, solicitar credenciales a **cgonzalezae@investigaciones.cl**

```bash
# URL (POST): /login
# body {"username": "username","password": "password"}
curl -s -H "Content-Type: application/json" -X POST -d '{ "username": "x", "password": "x"}' http://localhost:3000/login
{
    "message": "Success",
    "token": "xxxxxxxx.3SACGqx2tCskDCagxksU6_BFs33iq9OC2O59ys6K3lA"
}
```

Una vez que ya autenticó en el servidor, genera una cadena de caracteres que contiene la token de JWT integrada. Se usa esta cadena para enviar por medio de la cabecera de la peticion.

```bash
# URL (GET): /api/peoples\?rut\=008356282K\&nombre\=CAROLA\&sexo\=F\&origen\=EDF
# headers: access-token:xxxxxxxx.3SACGqx2tCskDCagxksU6_BFs33iq9OC2O59ys6K3lA
curl -s -H "access-token:xxxxxxxx.3SACGqx2tCskDCagxksU6_BFs33iq9OC2O59ys6K3lA" http://localhost:3000/api/peoples\?rut\=008356282K\&nombre\=CAROLA\&sexo\=F\&origen\=EDF
{
    "_id": "5ede36030a37c90017e09798",
    "cambios": [
        {
            "_id": "5ede5bf2cb0b85f21e0bc3f1",
            "created_at": "2020-06-08T15:40:34.601Z",
            "gener": "F",
            "name": "KAROLA",
            "origen": "EDF",
            "run": "008356282K",
            "updated_at": "2020-06-08T15:40:34.601Z"
        }
    ],
    "created_at": "2020-06-08T12:58:43.976Z",
    "gener": "F",
    "name": "CAROLA                                                                          ",
    "run": "008356282K",
    "updated_at": "2020-01-24T00:00:00.000Z"
}
```

### Postman

Configuracion de Postman para obtener el token

![login](/Users/Carlos/Desktop/ley21120v2/api/public/login.png)

Configuracin para capturar los datos de una persona.

![login](/Users/Carlos/Desktop/ley21120v2/api/public/people.png)

![](https://d33wubrfki0l68.cloudfront.net/7c21c4fdbd7dcc1632948c3590aba7bd62785cb9/949dc/assets-jekyll/blog/client-creds-with-spring-boot/client-creds-7fee4525b7b3e50e56ab635711468599b17126e8a8393986c572fffc2c4883b3.png)

