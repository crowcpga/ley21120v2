mongodump \
			--host mongo \
			--port 27017 \
			--db admin \
			--username $MONGO_INITDB_ROOT_USERNAME \
			--password $MONGO_INITDB_ROOT_PASSWORD \
			--archive=/backups/admin-`date +%m%d%Y`.gz --gzip