# Ley 21120 Programa de Identidad de Genero
<center>
	<strong>(PIG)</strong>
	<br/>
	<img src="api/public/mini-pig-manchas-morro.jpg" style="zoom:10%;" />
</center>



#### Diagramas de la arquitectura del proyecto.



##### FETCHER 

Este proceso se ejecuta por medio de un crontab, que es una tarea programada. el componente del sistema **fetcher** que es gatillado, se encarga de rescatar los archivos desde el FTP del registro civil y los trae a una carpeta en el servidor host por medio de un volumen compartido con el contenedor.

![](api/public/Ley21120-fetcher.png)

---

##### LOADER

Este proceso se ejecuta por medio de un crontab, que es una tarea programada. el componente del sistema **loader** que es gatillado, se encarga leer el contenido de cada archivo sincronizado. Cada uno de estos archivos exportados por el registro civil en formato COBOL se encuentran registros de personas que realizaron un cambio de genero, según la estructura de datos definida por el registro civil. Despues de leer cada registro ese es cargado a la base de datos que es un mongodb con un microservicio

<img src="api/public/EstructuraRC.png" style="zoom:40%;" />



![](api/public/Ley21120-loader.png)

---

##### API

El servicio de API es el encargado de entregar a los clientes la informacion solicitada, esta informacion en su index se muestra cual es la informacion requerida y como se comporta el servicio, pero en la entrega de informacion conectada a la base de datos se requiere una autentificacion (JWT) lo cual permite que solo las personas con autorizacion tengan acceso a dicha información, el middleware realiza la verificacion de la autentificación si esta es correcta dara acceso a las rutas detras de este componente. Una vez validada la autentificacion se rescata la ruta de get donde se validan los campos obligatorios y si está correcto extrae la información de la base de datos en Mongo y la entrega al los frontend. 



![](api/public/Ley21120-api.png)


##### Para levantar los servicios del proyecto.


```bash
# Para levantar los servicios del docker realizar en este orden para no tener problemas con la connexión con mongodb

# Levanta el microservicio de base de datos.
docker-compose up -d mongo

# Levanta el microservicio de backend API 
docker-compose up -d --build api 

```

##### Cuando la maquina se encuentre en estado UP se puede entrar a los servicios por ejemplo la URL a la API

Donde **$SERVER** se debe reemplar con el dominio o la IP del servidor y donde el **$PORT** se debe cambiar por el puerto configurado para el servicio

```php
#http://$SERVER:$PORT, ejemplo: 
http://127.0.0.1:3000
http://localhost:3000
```

##### Para trabajar con la Shell de API 

```bash
# Para entrar en la mongo shell
docker exec -it ley21120v2_api_1 /bin/bash
```

##### Para correr la sincronización de los archivos del registro civil en el servidor

```bash
# Ejecuta la copia de los archivos cargados en el registro civil a nuestra carpeta en el servidor, para su posterior carga en la base de datos.
# Este comando debe ser creado en un crontab para poder programar su ejecucion de forma automatica.
# Solo funcionará cuando se ejecute desde las IP autorizadas por el registro civil.
docker exec -it ley21120v2_fetcher_1 node fetcher.js
```

##### Para correr la carga de los datos a la base de datos. 

```bash
# Para cargar la informacion de los archivos de Registro Civil a la base de datos en MongoDB.
# Este comando debe ser creado en un crontab para poder programar su ejecucion de forma automatica.
docker exec -it ley21120v2_api_1 /bin/bash /etc/cron.hourly/loader
```

##### Para validar con pruebas unitarias.

```bash
# Para ejecutar las pruebas unitarias.
docker exec -it ley21120v2_api_1 /usr/local/bin/npm test
```

##### Para trabajar con la Shell de MongoDB.


```bash
# Para entrar en la mongo shell
docker exec -it ley21120v2_mongo_1 /bin/bash
```


```bash
# Para realizar un backup manualmente.
mongodump --host mongo --port 27017 --db admin --username root --password example --archive=/backups/admin-`date +%m%d%Y`.gz --gzip

# Para realizar una restauración manualmente.
mongorestore --host mongo --port 27017 --db admin --username root --password example --archive=/backups/admin-`date +%m%d%Y`.gz --gzip

```



##### TODO

- Habilitar (**usuario/contraseña**) por aplicación. 
- CI/CD with docker and gitlab