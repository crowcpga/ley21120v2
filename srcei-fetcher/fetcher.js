require('dotenv').config();
var Q = require('q');
let client = require('ssh2-sftp-client');
let sftp = new client();
const path = require('path');
var fs = require('fs');
const { ServerResponse } = require('http');
const SERVER_PORT = process.env.SERVER_PORT || 22;
const SERVER_PATH = process.env.SERVER_PATH || '/home/test/files';
const SERVER_HOST = process.env.SERVER_HOST || '10.134.1.75';
const SERVER_USER = process.env.SERVER_USER || 'test';
const SERVER_PASS = process.env.SERVER_PASS || '35a438a3b5n7fa1522a329484185ab95';
const LOCAL_DIR = process.env.LOCAL_DIR || '/app/srcei';
console.log(SERVER_USER + ':' + SERVER_PASS + '@' + SERVER_HOST + ':' + SERVER_PORT);
console.log("Conectando SFTP a " + SERVER_HOST + ":" + SERVER_PORT);
sftp.connect({
    host: SERVER_HOST,
    port: SERVER_PORT,
    username: SERVER_USER,
    password: SERVER_PASS
}).then(() => {
    console.log("Obteniendo listado de archivos");
    return sftp.list(SERVER_PATH);
}).then((data) => {
    var promises = [];
    for (var index in data) {
        var filename = data[index].name;
        if(fs.existsSync(path.posix.join(LOCAL_DIR, filename))){
            console.log(filename + " ya existe, no descargando nuevamente");
        } else {
            console.log("Descargando " +  filename);
            promises.push(sftp.get(path.posix.join(SERVER_PATH, filename), path.posix.join(LOCAL_DIR, filename)));
        }
    }
    return Q.all(promises);
}).then((data) => {
    console.log("Imprimiendo todos los archivos descargados.");
    console.log(data.toString());
    sftp.end();
    console.log("Terminado");
}).catch((error) => {
    console.log(error, 'catch error');
});