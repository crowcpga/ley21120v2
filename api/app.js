var express = require('express'),
    app = express(),
    jwt = require('jsonwebtoken'),
    bodyParser = require('body-parser'),
    validate = require('./utils/utils'),
    port = process.env.PORT || 3000;

const config = {
    key: '490a93fb87b8a8da14c6ffa4d3692686ee58ddf3',
}

// 1
app.set('key', config.key);

//2
app.use(bodyParser.urlencoded({ extended: true}));

// 3
app.use(bodyParser.json());

app.use(express.static('public'));

var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const MongoEnv = require('./models/MongoEnv');
mongoose.connect(new MongoEnv().createURLMongo());

var routes = require('./routers/router')
routes(app);

// 5
app.post('/login', (req, res) => {
    if (validate.validateLogin(req.body)) {
        const payload = {
            check: true
        }
        const token = jwt.sign(payload, app.get('key'), {
            expiresIn: 1440
        });
        res.json({
            message: 'Success',
            token: token
        })
    } else {
        res.json({
            message: 'Usuario o Contraseña, invalidos'
        })
    }
});

// 6
const rutasProtegidas = express.Router();
rutasProtegidas.use((req, res, next) => {
    const token = req.headers['access-token'];
    if (token) {
        jwt.verify(token, app.get('key'), (err, decoded) => {
            if (err) {
                return res.json({ mensaje: 'Token inválida' });
            } else {
                req.decoded = decoded;
                next();
            }
        });
    } else {
        res.send({
            mensaje: 'Token no proveída.'
        });
    }
});

// 7
var peopleController = require('./controllers/peoplesController');
app.get('/api/peoples/', rutasProtegidas, peopleController.get);

app.listen(port);

console.log('todo list RESTful API server started on: http://localhost:' + port);
