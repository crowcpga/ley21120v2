$(document).on('ready', function () {
    var Auth = {
        data: {
            username: 'ley21120',
            password: '1q2BL83VHm'
        }
    }
    var Utils = {
        outputHTML: function(html){
            $('#output code').html(html);
        },
        validateRut: function(){
            return $('#run').val() != "";
        }
    }
    var Handlers = {
        btnCleanClick: function(){
            Utils.outputHTML("\n");
        },
        formSubmit: function (e) {
            e.preventDefault();
            if (Utils.validateRut()) {
                var token = '';
                $.ajax({
                    url: 'login',
                    method: 'POST',
                    data: Auth.data,
                    success: function (data) {
                        token = data.token
                    } 
                }).then(function(){
                    $.ajax({
                        url: $('form.form-inline').attr('action'),
                        data: $('form.form-inline').serialize(),
                        headers: {
                            'Content-Type': 'application/json',
                            'access-token': token
                        },
                        success: function(data){
                            Utils.outputHTML(
                                "\n" + 
                                JSON.stringify(
                                    data, 
                                    null, 
                                    '    '
                                )
                            );
                        }
                    })
                });
            }
        }
    }
    var HandlerManagerItem = function(target, event, handler){
        this.target = target;
        this.event = event;
        this.handler = handler;
        $(target).on(event, handler);
    }
    new HandlerManagerItem('#btnClean', 'click', Handlers.btnCleanClick);
    new HandlerManagerItem('form.form-inline', 'submit', Handlers.formSubmit);
});
// Ofuscar : https://www.cleancss.com/javascript-obfuscate/index.php 