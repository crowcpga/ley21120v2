var { validateRequest } = require('../utils/utils');
var assert = require('assert');

describe("Test - validateRequests", function(){
    
    it("Check null case == false", function(){
        assert.equal(validateRequest(null), false);
    });

    it("Check {} case == false", function(){
        assert.equal(validateRequest({}), false);
    });

    it("Check lost required fields case == false", function(){
        assert.equal(validateRequest({
            query :{
                run: '008356282K'
            }
        }), false);
    });

    it("Check OK required fields but empty strings case == true", function(){
        assert.equal(validateRequest({
            query:{ 
                rut: '', 
                nombre: '', 
                sexo: '', 
                origen: '' 
            }
        }), false);
    });

    it("Check OK required fields case == true", function(){
        assert.equal(validateRequest({
            query:{ 
                rut: '008356282K', 
                nombre: 'CAROLA', 
                sexo: 'F', 
                origen: 'EDF' 
            }
        }), true);
    });

});