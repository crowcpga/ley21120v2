var { validateLogin } = require('../utils/utils');
var assert = require('assert');

describe("Test - validateLogin", function(){

    it("Check null case == false", function(){
        assert.equal(validateLogin(null), false);
    });

    it("Check {} case == false", function(){
        assert.equal(validateLogin({}), false);
    });

    it("Check login witch bad credentials case == false", function(){
        assert.equal(validateLogin({
            username: 'ley21120',
            password: 'ley21120'
        }), false);
    });
    
    it("Check login with good credentials case == true", function(){
        assert.equal(validateLogin({
            username: 'ley21120',
            password: '1q2BL83VHm'
        }), true);
    });
});