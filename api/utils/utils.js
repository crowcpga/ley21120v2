// Utils

exports.validateLogin = (auth) => {
    if (auth == null) return false;
    return auth.username == "ley21120" && auth.password == "1q2BL83VHm";
}

exports.validateRequest = function(request) {
    var fields = [
        "rut", 
        "nombre", 
        "sexo", 
        "origen"
    ];
    var valid = true;
    if (request == null) {
        return false;
    }
    if (Object.keys(request).length == 0){
        return false;
    }
    fields.forEach(field => {
        // validate field exist
        if (!request.query.hasOwnProperty(field)) {
            valid = false;
        } else {
            if (request.query[field].length == 0) {
                valid = false;
            }
        }
    });
    return valid;
}

exports.needRegistryRequestSystem = function(person,query){
    if (person.run.replace(/\s/g, "") != query.rut.replace(/\s/g, "")) return true;
    if (person.name.replace(/\s/g, "") != query.nombre.replace(/\s/g, "")) return true;
    if (person.gener.replace(/\s/g, "") != query.sexo.replace(/\s/g, "")) return true;
    return false;
}