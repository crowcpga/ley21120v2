'use strict'

const { validateRequest, needRegistryRequestSystem } = require('../utils/utils');
const People = require('../models/People');
const BadRequest = require('../models/BadRequest');
const path = require('path');

// /api/peoples/all
exports.peoplesAll = async function(request, response){
    const query = {},
        project = {},
        options = {
            sort: {
                created_at: -1
            },
            limit: 10
        },
        callback = function (error, peoples) {
            if (error) {
                response.send(error)
            } else {
                // console.log(peoples)
                response.json(peoples)
            }
        };
    const peoples = await People.find(
        query,
        project,
        options,
        callback
    );
}


exports.get = async function(request, response){
    // validate fields
    if (!validateRequest(request)){
        response.json({
            error: true,
            message: 'Revise los campos obligatorios. Revise la documentacion en http://localhost:3000/'
        });
        return false;
    }
    const query = { 
            // run: new RegExp(request.query.rut, 'i')  
            run: request.query.rut.padStart(10, '0')  
        }, 
        project = {}, 
        options = { 
            sort: { 
                created_at: -1 
            }
        },
        callback = async function(error, person){
            if (error || person == null) {
                response.send({
                    code: 404,
                    message: 'Person no found!'
                })
            } else {
                // registry System Requests.
                // if need registry new log.
                if (needRegistryRequestSystem(person, request.query)){
                    // validateBadRequest entry pre-exists
                    var params = {
                        run: request.query.rut.padStart(10, '0'),
                        name: request.query.nombre,
                        gener: request.query.sexo,
                        origen: request.query.origen
                    }

                    let badRequests = await BadRequest.find(params,async function(error, docs){
                        if (!docs.length){
                            let badRequest = new BadRequest(params);
                            const now = Date.now();
                            badRequest.created_at = now;
                            badRequest.updated_at = now;
                            await badRequest.save();    
                        }
                    });
                }
                // get logs from rut and origen
                const logs = await BadRequest.find({ 
                    run: request.query.rut, 
                    origen: request.query.origen 
                });
                person.cambios = logs;
                response.json(person)
            }
        };
    await People.findOne(
        query, 
        project, 
        options,
        callback
    );
}

exports.index = function(request, response) {
    response.sendFile(path.join(__dirname + '/../views/index.html'));
}

exports.pdf = function(request, response){
    response.sendFile(path.join(__dirname + '/../views/Informacion_Identidad_De_Genero_V4.pdf'));
}

exports.jwt = function (request, response) {
    response.sendFile(path.join(__dirname + '/../views/JWT.pdf'));
}

exports.estructura_tecnica = function (request, response) {
    response.sendFile(path.join(__dirname + '/../views/estructura_tecnica.pdf'));
}