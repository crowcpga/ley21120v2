const mongoose = require('mongoose');
const { Schema } = mongoose;
module.exports = mongoose.model('File', new Schema({
    filename: String,
    path: String,
    count: Number,
    status: String, 
    created_at: Date,
    finished_at: Date
}));