const mongoose = require('mongoose');

const { Schema } = mongoose;

// model
module.exports = mongoose.model('BadRequest', new Schema({
    run: String,
    name: String,
    gener: String,
    origen: String,
    updated_at: Date,
    created_at: Date,
    __v: {
        type: Schema.Types.Decimal128,
        select: false
    }
}));