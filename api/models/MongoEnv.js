require('dotenv').config();
module.exports = class MongoEnv {
    createURLMongo() {
        const SERVER_PORT = process.env.SERVER_PORT || 22;
        const SERVER_HOST = process.env.SERVER_HOST || '10.134.1.75';
        const SERVER_USER = process.env.SERVER_USER || 'test';
        const SERVER_PASS = process.env.SERVER_PASS || '35a438a3b5b7fa1522a329484185ab95';
        const connectionString =  "mongodb://" + SERVER_USER + ":" + SERVER_PASS + "@" + SERVER_HOST + ":" + SERVER_PORT + "/?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false";
        console.log(connectionString);
        return connectionString;
    }
}