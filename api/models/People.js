const mongoose = require('mongoose');

const { Schema } = mongoose;

// model
module.exports = mongoose.model('People', new Schema({
    run: String,
    name: String,
    gener: String,
    updated_at: Date,
    created_at: Date,
    cambios: Array,
    from_filename: {
        type: String,
        select: false
    },
    __v: {
        type: Schema.Types.Decimal128,
        select: false
    }
}));