// const MongoEnv = require('./../models/MongoEnv');
const MongoEnv = require('./../models/MongoEnv');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect(new MongoEnv().createURLMongo())

const LoaderService = require('./services/LoaderService');
const service = new LoaderService();
service.load();