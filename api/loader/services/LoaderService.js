const FileLoaderService = require('../services/FileLoaderService');
module.exports = class LoaderService {
    load() {
        const service = new FileLoaderService('data');
        service.load();
    }
}