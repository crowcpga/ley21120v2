let fs = require('fs');
const mongoose = require('mongoose');
const People = require('./../../models/People');
const File = require('./../../models/File');

class FileLoaderService {

    constructor(path) {
        this.path = path;
    }

    static extractData(line) {
        let run = line.substring(0, 10);
        line = line.substring(10);
        let name = line.substring(0, 80);
        line = line.substring(80);
        let gener = line.substring(0, 1);
        line = line.substring(1);
        let updated_at = line.substring(0, 10);
        let updated_string = updated_at;

        let year = updated_at.substring(0, 4);
        updated_at = updated_at.substring(4);
        let month = updated_at.substring(0, 2);
        updated_at = updated_at.substring(2);
        let day = updated_at.substring(0, 2);
        updated_at = updated_at.substring(2);
        
        // console.log(year, month, day);

        line = line.substring(10);
        return {
            run: run,
            name: name,
            gener: gener,
            updated_at: new Date(year, month - 1 , day),
            updated_string: updated_string
        };
    }

    validateCompress(filename) {
        return filename.includes(".gz")
    }

    closeConnection() {
        mongoose.connection.close(() => console.log('connection close'));
    }

    load() {
        const path = this.path;
        const self = this;
        fs.readdir(path, async function(err, items){
            for(var i = 0; i < items.length; i++) {
                const filename = items[i];
                const entries = [];

                const now = Date.now();

                // validate no compress files. 
                if (self.validateCompress(filename)){

                    console.error("Documento comprimido " + filename + ", no se puede gestionar aun.");

                } else {

                    // check 
                    const file = await File.findOne({ filename: filename });
                    if (!file) {
                        const file = new File({
                            path: path,
                            filename: filename,
                            status: 'created',
                            created_at: now
                        });
                        await file.save();

                        fs.readFileSync(path + '/' + filename, 'utf-8').split(/\r?\n/).forEach(function (line) {
                            if (line != "") {
                                let object = FileLoaderService.extractData(line);
                                // console.log(object); 
                                entries.push(object);
                            }
                        });

                        if (entries.length > 0) {
                            entries.forEach(async function (entry, index) {
                                console.log((index + 1) + ' of ' + entries.length);
                                const people = new People(entry);
                                people.created_at = now;
                                people.from_filename = file.filename;
                                await people.save();
                            });
                        } else {
                            console.log("El archivo " + filename + " no tiene registros")
                        }

                        // saved file when is completed
                        file.status = 'saved';
                        file.count = entries.length;
                        file.finished_at = Date.now();
                        await file.save();
                        console.log(filename + ' guardado...');

                    } else {
                        console.log(path + '/' + filename + ' ya existe en la base de datos. ');
                    }

                }

            }

            self.closeConnection();
        });
        
    }

}

module.exports = FileLoaderService;
