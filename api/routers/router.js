'use strict'

module.exports = function(app) {
    var peopleController = require('../controllers/peoplesController');
    app.route('/api/peoples/all')
        .get(peopleController.peoplesAll);
    app.route('/')
        .get(peopleController.index);
    app.route('/pdf')
        .get(peopleController.pdf);
    app.route('/JWT.pdf')
        .get(peopleController.jwt);
    app.route('/estructura_tecnica.pdf')
        .get(peopleController.estructura_tecnica);
}