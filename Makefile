up:
	docker-compose up -d mongo 
	docker-compose up -d --build api 
	docker-compose up -d --build fetcher 

down: 
	docker-compose down

loader:
	docker exec -it ley21120v2_api_1 /bin/bash /etc/cron.hourly/loader

fetcher: 
	docker exec -it ley21120v2_fetcher_1 node fetcher.js

remove: down
	docker rmi -f ley21120v2_api	

api_bash:
	docker exec -it ley21120v2_api_1 /bin/bash

mongo_bash:
	docker exec -it ley21120v2_mongo_1 /bin/bash

mongo_backup:
	docker exec -it ley21120v2_mongo_1 /bin/bash /etc/cron.daily/backup

get_token:
	curl -s -H "Content-Type: application/json" -X POST -d '{ "username": "ley21120", "password": "1q2BL83VHm"}' http://localhost:3000/login | python -m json.tool

get_person:
	curl -s -H "access-token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaGVjayI6dHJ1ZSwiaWF0IjoxNTkxNjMzMTYzLCJleHAiOjE1OTE2MzQ2MDN9.3SACGqx2tCskDCagxksU6_BFs33iq9OC2O59ys6K3lA" http://localhost:3000/api/peoples\?rut\=008356282K\&nombre\=CAROLA\&sexo\=F\&origen\=EDF | python -m json.tool
